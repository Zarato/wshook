﻿using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.Text;

namespace WSHook.IO.Cache
{
    // From https://github.com/thenameless314159/Astron/blob/master/src/Astron.Binary/Cache/PrimitiveBinaryCacheBuilder.cs
    internal static class PrimitiveBinaryCacheBuilder
    {
        public static bool IsAlreadySet { get; private set; }

        private static unsafe void Register<T>(
            PrimitiveBinaryCache<T>.ReadDelegate read,
            PrimitiveBinaryCache<T>.WriteDelegate write)
            where T : unmanaged
        {
            PrimitiveBinaryCache<T>.SizeOf = sizeof(T);
            PrimitiveBinaryCache<T>.ReadValue = read;
            PrimitiveBinaryCache<T>.WriteValue = write;
            PrimitiveBinaryCache<T>.CompleteRegistration();
        }

        static PrimitiveBinaryCacheBuilder() // register readByte
        {
            Register(src => src[0], (dst, val) => dst[0] = val);
            Register(src => unchecked((sbyte)src[0]), (dst, val) => dst[0] = unchecked((byte)val));
            Register(src => src[0] != 0, (dst, val) => dst[0] = dst[0] = val ? (byte)1 : (byte)0);

        }

        public static void RegisterLittleEndian()
        {
            Register(BinaryPrimitives.ReadInt16LittleEndian, BinaryPrimitives.WriteInt16LittleEndian);
            Register(BinaryPrimitives.ReadInt32LittleEndian, BinaryPrimitives.WriteInt32LittleEndian);
            Register(BinaryPrimitives.ReadInt64LittleEndian, BinaryPrimitives.WriteInt64LittleEndian);
            Register(BinaryPrimitives.ReadUInt16LittleEndian, BinaryPrimitives.WriteUInt16LittleEndian);
            Register(BinaryPrimitives.ReadUInt32LittleEndian, BinaryPrimitives.WriteUInt32LittleEndian);
            Register(BinaryPrimitives.ReadUInt64LittleEndian, BinaryPrimitives.WriteUInt64LittleEndian);

            Register(src => BitConverter.Int32BitsToSingle(BinaryPrimitives.ReadInt32LittleEndian(src)),
                     (dst, val) => BinaryPrimitives.WriteInt32BigEndian(dst, BitConverter.SingleToInt32Bits(val)));
            Register(src => BitConverter.Int64BitsToDouble(BinaryPrimitives.ReadInt64BigEndian(src)),
                     (dst, val) => BinaryPrimitives.WriteInt64BigEndian(dst, BitConverter.DoubleToInt64Bits(val)));


            IsAlreadySet = true;
        }
    }
}
