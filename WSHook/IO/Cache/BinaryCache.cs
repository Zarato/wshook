﻿using System;
using System.Collections.Generic;
using System.Text;
using WSHook.IO.Reader;
using WSHook.IO.Writer;

namespace WSHook.IO.Cache
{
    // From https://github.com/thenameless314159/Astron/blob/master/src/Astron.Binary/Cache/BinaryCache.cs
    internal static class BinaryCache<T>
    {
        private static bool _isReaderAlreadyRegistered;
        private static bool _isWriterAlreadyRegistered;
        private static bool _isSizeOfAlreadyRegistered;

        private static Func<IReader, T> _reader;
        private static Action<IWriter, T> _writer;
        private static Func<T, int> _sizeOf;

        public static Func<T, int> SizeOf
        {
            get => _sizeOf ?? throw new KeyNotFoundException(
                $"{nameof(SizeOf)} cache of {typeof(T).Name} haven't been registered in the current {nameof(IReader)}.");
            set
            {
                if (_isSizeOfAlreadyRegistered) throw new InvalidOperationException(
                    $"{nameof(SizeOf)} cache of {typeof(T).Name} has already been registered in another {nameof(IReader)}.");

                _sizeOf = value;
                _isSizeOfAlreadyRegistered = true;
            }
        }

        public static Func<IReader, T> ReadValue
        {
            get => _reader ?? throw new KeyNotFoundException(
                $"{nameof(ReadValue)} cache of {typeof(T).Name} haven't been registered in the current {nameof(IReader)}.");
            set
            {
                if (_isReaderAlreadyRegistered) throw new InvalidOperationException(
                    $"{nameof(ReadValue)} cache of {typeof(T).Name} has already been registered in another {nameof(IReader)}.");

                _reader = value;
                _isReaderAlreadyRegistered = true;
            }
        }

        public static Action<IWriter, T> WriteValue
        {
            get => _writer ?? throw new KeyNotFoundException(
                $"{nameof(WriteValue)} cache of {typeof(T).Name} haven't been registered in the current {nameof(IWriter)}.");
            set
            {
                if (_isWriterAlreadyRegistered) throw new InvalidOperationException(
                    $"{nameof(WriteValue)} cache of {typeof(T).Name} has already been registered in another {nameof(IWriter)}.");

                _writer = value;
                _isWriterAlreadyRegistered = true;
            }
        }
    }
}
