﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace WSHook.IO.Cache
{
    // From https://github.com/thenameless314159/Astron/blob/master/src/Astron.Binary/Cache/PrimitiveTypes.cs
    public static class PrimitiveTypes
    {
        public static ImmutableList<Type> Primitives = ImmutableList
            .Create(typeof(byte), typeof(sbyte), typeof(bool), typeof(short), typeof(ushort),
                typeof(int), typeof(uint), typeof(long), typeof(ulong), typeof(float), typeof(double),
                typeof(decimal));
    }
}
