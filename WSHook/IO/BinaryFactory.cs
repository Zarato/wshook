﻿using System;
using WSHook.IO.Reader;
using WSHook.IO.Writer;

namespace WSHook.IO
{
    // From https://github.com/thenameless314159/Astron/blob/master/src/Astron.Binary/BinaryFactory.cs
    public class BinaryFactory : IBinaryFactory
    {

        public IReader Get(byte[] buffer) => new MemoryReader(buffer);

        public IReader Get(Memory<byte> buffer) => new MemoryReader(buffer);

        public IReader Get(ReadOnlyMemory<byte> buffer) => new MemoryReader(buffer);

        public IWriter Get(int length) => new MemoryWriter(new SimpleMemoryOwner<byte>(new byte[length]), length);
    }
}
