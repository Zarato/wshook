﻿using System;
using System.Collections.Generic;
using System.Text;
using WSHook.IO.Cache;
using WSHook.IO.Storage;

namespace WSHook.IO
{
    // From https://github.com/thenameless314159/Astron/blob/master/src/Astron.Binary/BinaryBuilder.cs
    public class BinaryBuilder : IBinaryBuilder
    {
        public BinaryBuilder()
        {
            if (PrimitiveBinaryCacheBuilder.IsAlreadySet) return;

            PrimitiveBinaryCacheBuilder.RegisterLittleEndian();
        }

        public IBinaryBuilder Register<T>(IWriterStorage<T> storage)
        {
            BinaryCache<T>.WriteValue = storage.WriteValue;
            return this;
        }

        public IBinaryBuilder Register<T>(IReaderStorage<T> storage)
        {
            BinaryCache<T>.ReadValue = storage.ReadValue;
            return this;
        }

        public IBinaryBuilder Register<T>(Func<T, int> func)
        {
            BinaryCache<T>.SizeOf = func;
            return this;
        }

        public IBinaryBuilder Register<T>(IBinaryStorage<T> storage) => this
            .Register((IWriterStorage<T>)storage)
            .Register((IReaderStorage<T>)storage);

        public IBinaryFactory Build() => new BinaryFactory();
    }
}
