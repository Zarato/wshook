﻿using System;
using System.Collections.Generic;
using System.Text;
using WSHook.IO.Reader;

namespace WSHook.IO.Storage
{
    // From https://github.com/thenameless314159/Astron/blob/master/src/Astron.Binary/Storage/IReaderStorage.cs
    public interface IReaderStorage<out T>
    {
        Func<IReader, T> ReadValue { get; }
    }
}
