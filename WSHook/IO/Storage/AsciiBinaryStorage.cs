﻿using System;
using System.Text;
using WSHook.IO.Reader;
using WSHook.IO.Writer;

namespace WSHook.IO.Storage
{
    // From https://github.com/thenameless314159/Astron/blob/master/src/Astron.Binary/Storage/AsciiBinaryStorage.cs
    public class AsciiBinaryStorage : IBinaryStorage<string>
    {
        public Func<IReader, string> ReadValue => reader =>
        {
            var length = reader.ReadValue<int>();

            if (length < 1) return string.Empty;
            var encodedStr = reader.GetSlice(length);
            reader.Advance(length);
            return Encoding.ASCII.GetString(encodedStr.Span);
        };

        public Action<IWriter, string> WriteValue => (writer, value) =>
        {
            if (value == string.Empty) return;

            var encodedStr = Encoding.ASCII.GetBytes(value);
            writer.WriteValue(encodedStr.Length);
            writer.WriteBytes(encodedStr);
        };
    }
}
