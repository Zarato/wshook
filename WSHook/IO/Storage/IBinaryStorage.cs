﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSHook.IO.Storage
{
    // From https://github.com/thenameless314159/Astron/blob/master/src/Astron.Binary/Storage/IBinaryStorage.cs
    public interface IBinaryStorage<T> : IReaderStorage<T>, IWriterStorage<T>
    {
    }
}
