﻿using System;
using System.Collections.Generic;
using System.Text;
using WSHook.IO.Writer;

namespace WSHook.IO.Storage
{
    // From https://github.com/thenameless314159/Astron/blob/master/src/Astron.Binary/Storage/IWriterStorage.cs
    public interface IWriterStorage<in T>
    {
        Action<IWriter, T> WriteValue { get; }
    }
}
