﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WSHook.Native
{
    internal static class Kernel32
    {
        /// <summary>
        /// Determines whether the specified process is running under WOW64 or an Intel64 of x64 processor.
        /// </summary>
        /// <param name="hProcess">A handle to the process. The handle must have the PROCESS_QUERY_INFORMATION or PROCESS_QUERY_LIMITED_INFORMATION access right.</param>
        /// <param name="Wow64Process">A pointer to a value that is set to TRUE if the process is running under WOW64 on an Intel64 or x64 processor. If the process is running under 32-bit Windows, the value is set to FALSE. If the process is a 32-bit application running under 64-bit Windows 10 on ARM, the value is set to FALSE. If the process is a 64-bit application running under 64-bit Windows, the value is also set to FALSE.</param>
        /// <returns>If the function succeeds, the return value is a nonzero value.</returns>
        [NativeMethod("kernel32.dll", Init = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate bool IsWow64Process(IntPtr hProcess, out bool Wow64Process);
    }
}
