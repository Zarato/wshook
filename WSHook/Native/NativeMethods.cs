﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using WSHook.Extensions;

namespace WSHook.Native
{
    internal static class NativeMethods
    {

        private static Dictionary<Type, Delegate> _delegates;

        static NativeMethods()
        {
            // retrieve all native methods in the current assembly
            var types = Assembly.GetExecutingAssembly()
                                .GetTypes()
                                .Where(t => t.HasAttribute<NativeMethodAttribute>());
            _delegates = new Dictionary<Type, Delegate>();

            foreach (var nativeMethod in types)
            {
                var attribute = nativeMethod.GetCustomAttribute<NativeMethodAttribute>();

                if (attribute.Init)
                    _delegates.Add(nativeMethod,
                                   NativeHelper.CreateDelegate(attribute.Dll, attribute.Method, nativeMethod));
            }
        }

        /// <summary>
        /// Get a delegate for the specified unmanaged method.
        /// </summary>
        /// <param name="dll">Module.</param>
        /// <param name="method">Method.</param>
        /// <param name="type">Delegate type.</param>
        /// <returns>A delegate to the unmanaged method.</returns>
        public static Delegate Get(string dll, string method, Type type)
        {
            if (!_delegates.TryGetValue(type, out Delegate nativeMethod))
            {
                nativeMethod = NativeHelper.CreateDelegate(dll, method, type);
                _delegates.Add(type, nativeMethod);
            }
            return nativeMethod;
        }

        /// <summary>
        /// Get a delegate for the specified unmanaged method.
        /// </summary>
        /// <param name="type">Delegate type. Must have <see cref="NativeMethodAttribute"/> attribute.</param>
        /// <returns>A delegate to the unmanaged method.</returns>
        public static Delegate Get(Type type)
        {
            if (!type.HasAttribute<NativeMethodAttribute>())
                throw new ArgumentException($"{type.Name} has not the attribute {nameof(NativeMethodAttribute)}.");
            var attribute = type.GetCustomAttribute<NativeMethodAttribute>();
            return Get(attribute.Dll, attribute.Method, type);
        }

        /// <summary>
        /// Get a delegate for the specified unmanaged method.
        /// </summary>
        /// <typeparam name="T">Delegate type.</typeparam>
        /// <param name="dll">Module.</param>
        /// <param name="method">Method.</param>
        /// <returns>A delegate to the unmanaged method.</returns>
        public static T Get<T>(string dll, string method) where T : Delegate => (T)Get(dll, method, typeof(T));

        /// <summary>
        /// Get a delegate for the specified unmanaged method.
        /// </summary>
        /// <typeparam name="T">Delegate type. Must have <see cref="NativeMethodAttribute"/> attribute.</typeparam>
        /// <returns>A delegate to the unmanaged method.</returns>
        public static T Get<T>() where T : Delegate => (T)Get(typeof(T));
    }
}
