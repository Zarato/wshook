﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using static WSHook.Native.NativeConstants;

namespace WSHook.Native
{
    internal static class Ntdll
    {

        /// <summary>
        /// This function copies the specified address range from the specified process into the specified address range of the current process.
        /// </summary>
        /// <param name="ProcessHandle">Supplies an open handle to a process object.</param>
        /// <param name="BaseAddress">Supplies the base address in the specified process to be read.</param>
        /// <param name="Buffer">Supplies the address of a buffer which receives the contents from the specified process address space.</param>
        /// <param name="NumberOfBytesToRead">Supplies the requested number of bytes to read from the specified process.</param>
        /// <param name="NumberOfBytesReaded">Receives the actual number of bytes transferred into the specified buffer.</param>
        /// <returns>If the function succeed, return true.</returns>
        [NativeMethod("ntdll.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate bool NtReadVirtualMemory(IntPtr ProcessHandle, IntPtr BaseAddress, out byte[] Buffer, int NumberOfBytesToRead, out int NumberOfBytesReaded);

        /// <summary>
        /// This function creates a region of pages within the virtual address space of a subject process.
        /// </summary>
        /// <param name="ProcessHandle">Supplies an open handle to a process object.</param>
        /// <param name="BaseAddress">Supplies a pointer to a variable that will receive the base address of the allocated region of pages.
        /// If the initial value of this argument is not null, then the region will be allocated starting at the specified virtual address rounded down to the next
        /// host page size address boundary.
        /// If the initial value of this argument is null, then the operating system will determine where to allocate theregion.</param>
        /// <param name="ZeroBits">Supplies the number of high order address bits that must be zero in the base address of the section view.
        /// The value of this argument must be less than 21 and is only used when the operating system determines where to allocate the view (i.e.when BaseAddress is null).</param>
        /// <param name="RegionSize">Supplies a pointer to a variable that will receive the actual size in bytes of the allocated region of pages.
        /// The initial value of this argument specifies the size in bytes of the region and is rounded up to the next host page size boundary.</param>
        /// <param name="AllocationType">Supplies a set of flags that describe the type of allocation that is to be performed for the specified region of pages.</param>
        /// <param name="Protect">Supplies the protection desired for the committed region of pages.</param>
        /// <returns>If the function succeed, return true.</returns>
        [NativeMethod("ntdll.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate bool NtAllocateVirtualMemory(IntPtr ProcessHandle, ref IntPtr BaseAddress, int ZeroBits, ref int RegionSize, int AllocationType, int Protect);

        /// <summary>
        /// This function deletes a region of pages within the virtual address space of a subject process.
        /// </summary>
        /// <param name="ProcessHandle">An open handle to a process object.</param>
        /// <param name="BaseAddress">The base address of the region of pages  to be freed.
        /// This value is rounded down to the next host page address boundary.</param>
        /// <param name="RegionSize">A pointer to a variable that will receive the actual size in bytes of the freed region of pages.
        /// The initial value of this argument is rounded up to the next host page size boundary.</param>
        /// <param name="FreeType">A set of flags that describe the type of free that is to be performed for the specified region of pages.</param>
        /// <returns>If the function succeed, return true.</returns>
        [NativeMethod("ntdll.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate bool NtFreeVirtualMemory(IntPtr ProcessHandle, ref IntPtr BaseAddress, out int RegionSize, int FreeType);

        /// <summary>
        /// This routine changes the protection on a region of committed pageswithin the virtual address space of the subject process.
        /// Setting the protection on a ragne of pages causes the old protection to be replaced by the specified protection value.
        /// </summary>
        /// <param name="ProcessHandle">An open handle to a process object.</param>
        /// <param name="BaseAddress">The base address of the region of pages whose protection is to be changed.
        /// This value is rounded down to the next host page address boundary.</param>
        /// <param name="RegionSize">A pointer to a variable that will receive the actual size in bytes of the protected region of pages.
        /// The initial value of this argument is rounded up to the next host page size boundary.</param>
        /// <param name="NewProtect">The new protection desired for the specified region of pages.</param>
        /// <param name="OldProtect">A pointer to a variable that will receive the old protection of the first page within the specified region of pages.</param>
        /// <returns>If the function succeed, return true.</returns>
        [NativeMethod("ntdll.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate bool NtProtectVirtualMemory(IntPtr ProcessHandle, ref IntPtr BaseAddress, ref int RegionSize, int NewProtect, out int OldProtect);

        /// <summary>
        /// This function copies the specified address range from the current process into the specified address range of the specified process.
        /// </summary>
        /// <param name="ProcessHandle">Supplies an open handle to a process object.</param>
        /// <param name="BaseAddress">Supplies the base address to be written to in the specified process.</param>
        /// <param name="Buffer">Supplies the address of a buffer which contains the contents to be written into the specified process address space.</param>
        /// <param name="NumberOfBytesToWrite">Supplies the requested number of bytes to write into the specified process.</param>
        /// <param name="NumberOfBytesWritten">Receives the actual number of bytes transferred into the specified address space.</param>
        /// <returns>If the function succeed, return true.</returns>
        [NativeMethod("ntdll.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate bool NtWriteVirtualMemory(IntPtr ProcessHandle, IntPtr BaseAddress, byte[] Buffer, int NumberOfBytesToWrite, out int NumberOfBytesWritten);

        /// <summary>
        /// Retrieves various kinds of object information.
        /// </summary>
        /// <param name="Handle">The handle of the object for which information is being queried.</param>
        /// <param name="ObjectInformationClass">One of the following values, as enumerated in OBJECT_INFORMATION_CLASS, indicating the kind of object information to be retrieved.</param>
        /// <param name="ObjectInformation">An optional pointer to a buffer where the requested information is to be returned. The size and structure of this information varies depending on the value of the ObjectInformationClass parameter.</param>
        /// <param name="ObjectInformationLength">The size of the buffer pointed to by the ObjectInformation parameter, in bytes.</param>
        /// <param name="ReturnLength">An optional pointer to a location where the function writes the actual size of the information requested. If that size is less than or equal to the ObjectInformationLength parameter, the function copies the information into the ObjectInformation buffer; otherwise, it returns an NTSTATUS error code and returns in ReturnLength the size of the buffer required to receive the requested information.</param>
        /// <returns>Returns an NTSTATUS or error code.</returns>
        [NativeMethod("ntdll.dll", Init = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate bool NtQueryObject(IntPtr Handle, OBJECT_INFORMATION_CLASS ObjectInformationClass, IntPtr ObjectInformation, uint ObjectInformationLength, out uint ReturnLength);
    }
}
