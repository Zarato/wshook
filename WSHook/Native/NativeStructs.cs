﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WSHook.Native
{
    public class NativeStructs
    {

        [StructLayout(LayoutKind.Sequential)]
        public struct PUBLIC_OBJECT_BASIC_INFORMATION
        {
            public uint Attributes;
            public uint GrantedAccess;
            public uint HandleCount;
            public uint PointerCount;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public uint[] Reserved;
        }
    }
}
