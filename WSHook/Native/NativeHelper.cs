﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using WSHook.Exceptions;

namespace WSHook.Native
{
    internal static class NativeHelper
    {
        private static Dictionary<string, IntPtr> _moduleHandles;

        /// <summary>
        /// Loads the specified module into the address space of the calling process. The specified module may cause other modules to be loaded.
        /// </summary>
        /// <param name="lpFileName">The name of the module. This can be either a library module (a .dll file) or an executable module (an .exe file). The name specified is the file name of the module and is not related to the name stored in the library module itself, as specified by the LIBRARY keyword in the module-definition (.def) file.</param>
        /// <returns>If the function succeeds, the return value is a handle to the module.</returns>
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Ansi)]
        static extern IntPtr LoadLibrary([MarshalAs(UnmanagedType.LPStr)]string lpFileName);

        /// <summary>
        /// Retrieves the address of an exported function or variable from the specified dynamic-link library (DLL).
        /// </summary>
        /// <param name="hModule">A handle to the DLL module that contains the function or variable. The LoadLibrary, LoadLibraryEx, LoadPackagedLibrary, or GetModuleHandle function returns this handle.</param>
        /// <param name="procName">The function or variable name, or the function's ordinal value. If this parameter is an ordinal value, it must be in the low-order word; the high-order word must be zero.</param>
        /// <returns>If the function succeeds, the return value is the address of the exported function or variable.</returns>
        [DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        static extern IntPtr GetProcAddress(IntPtr hModule, string procName);


        static NativeHelper()
        {
            _moduleHandles = new Dictionary<string, IntPtr>();
            var modules = Process.GetCurrentProcess().Modules;
            foreach (ProcessModule module in modules)
            {
                if (!_moduleHandles.ContainsKey(module.ModuleName))
                    _moduleHandles.Add(module.ModuleName, module.BaseAddress);
            }
        }

        /// <summary>
        /// Create a delegate for an unmanaged method.
        /// </summary>
        /// <typeparam name="T">Delegate.</typeparam>
        /// <param name="dll">Module.</param>
        /// <param name="method">Method name.</param>
        /// <returns></returns>
        public static T CreateDelegate<T>(
            string dll,
            string method) where T : Delegate => (T)CreateDelegate(dll, method, typeof(T));

        /// <summary>
        /// Create a delegate for an unmanaged method.
        /// </summary>
        /// <param name="dll">Module.</param>
        /// <param name="method">Method name.</param>
        /// <param name="typeDelegate">Delegate.</param>
        /// <returns></returns>
        public static Delegate CreateDelegate(string dll, string method, Type typeDelegate)
        {
            IntPtr moduleHandle;
            if (!_moduleHandles.TryGetValue(dll, out moduleHandle))
            {
                moduleHandle = LoadLibrary(dll);
            }

            if (moduleHandle == IntPtr.Zero)
                throw new InvalidModuleException(dll);

            IntPtr methodHandle = GetProcAddress(moduleHandle, method);
            if (methodHandle == IntPtr.Zero)
                throw new InvalidMethodException(method, dll);

            return Marshal.GetDelegateForFunctionPointer(methodHandle, typeDelegate);
        }
    }
}
