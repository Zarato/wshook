﻿namespace WSHook.Native
{
    public static class NativeConstants
    {
        public const long SYNCHRONIZE = 0x00100000;
        public const long PROCESS_CREATE_THREAD = 0x0002;
        public const long PROCESS_DUP_HANDLE = 0x0040;
        public const long PROCESS_QUERY_INFORMATION = 0x0400;
        public const long PROCESS_SET_INFORMATION = 0x0200;
        public const long PROCESS_SUSPEND_RESUME = 0x0800;
        public const long PROCESS_TERMINATE = 0x0001;
        public const long PROCESS_VM_OPERATION = 0x0008;
        public const long PROCESS_VM_READ = 0x0010;
        public const long PROCESS_VM_WRITE = 0x0020;

        public const long MEM_COMMIT = 0x00001000;
        public const long MEM_RESERVE = 0x00002000;
        public const long MEM_RESET = 0x00080000;
        public const long MEM_RESET_UNDO = 0x1000000;

        public const long PAGE_EXECUTE = 0x10;
        public const long PAGE_EXECUTE_READ = 0x20;
        public const long PAGE_EXECUTE_READWRITE = 0x40;
        public const long PAGE_EXECUTE_WRITECOPY = 0x80;
        public const long PAGE_NOACCESS = 0x01;
        public const long PAGE_READONLY = 0x02;
        public const long PAGE_READWRITE = 0x04;
        public const long PAGE_WRITECOPY = 0x08;
        public const long PAGE_GUARD = 0x100;
        public const long PAGE_NOCACHE = 0x200;

        public enum OBJECT_INFORMATION_CLASS : int
        {
            ObjectBasicInformation = 0,
            ObjectNameInformation = 1,
            ObjectTypeInformation = 2,
            ObjectAllTypesInformation = 3,
            ObjectHandleInformation = 4
        }
    }
}
