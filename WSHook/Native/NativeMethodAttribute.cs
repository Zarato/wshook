﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace WSHook.Native
{
    [AttributeUsage(AttributeTargets.Delegate)]
    internal class  NativeMethodAttribute : Attribute
    {
        public string Dll { get; }
        public string Method { get; }
        public bool Init { get; set; }

        public NativeMethodAttribute(string dll, [CallerMemberName] string method = "")
        {
            if (dll == "")
            {
                throw new NotImplementedException();
                /*
                 * StackTrace trace = new StackTrace();
                 * Console.WriteLine(trace.FrameCount);
                 * Console.WriteLine(trace.GetFrame(trace.FrameCount - 1).GetMethod().DeclaringType.Name);
                 * still buggy cause .NET core and .NET Framework are differents
                 */
            }
            Dll = dll;
            Method = method;
        }
    }
}
