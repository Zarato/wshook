﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using WSHook.Exceptions;
using WSHook.Hooks;
using WSHook.Injection;

namespace WSHook
{
    public class HookManager : IDisposable
    {
        private List<IHook> _hooks;

        /// <summary>
        /// Name of the pipe on which the app and the dll will communicate.
        /// </summary>
        public string PipeName { get; }

        /// <summary>
        /// The process you want to apply hooks.
        /// </summary>
        public Process Process { get; }

        /// <summary>
        /// Path to the x86 dll.
        /// </summary>
        public string X86DLL { get; }

        /// <summary>
        /// Path to the x64 dll.
        /// </summary>
        public string X64DLL { get; }

        /// <summary>
        /// An instance of the injector.
        /// </summary>
        public IInjector Injector { get; private set; }

        /// <summary>
        /// Indicates if the dll is injected into the process.
        /// </summary>
        public bool IsInjected => Injector == null || Injector.IsInjected;

        /// <summary>
        /// Instance of <see cref="Memory"/> for virtual memory operations.
        /// </summary>
        public Memory ProcessMemory { get; set; }


        /// <summary>
        /// Create an instance of <see cref="HookManager"/>
        /// </summary>
        /// <param name="process">The process you want to apply hooks.</param>
        /// <param name="x86dll">Path to the x86 dll.</param>
        /// <param name="x64dll">Path to the x64 dll.</param>
        public HookManager(
            Process process,
            string x86dll,
            string x64dll) : this("WSHook", process, x86dll, x64dll)
        {
        }

        /// <summary>
        /// Create an instance of <see cref="HookManager"/>
        /// </summary>
        /// <param name="pid">Process id of the process you want to apply hooks.</param>
        /// <param name="x86dll">Path to the x86 dll.</param>
        /// <param name="x64dll">Path to the x64 dll.</param>
        public HookManager(
            int pid,
            string x86dll,
            string x64dll) : this(Process.GetProcessById(pid), x86dll, x64dll)
        {
        }

        /// <summary>
        /// Create an instance of <see cref="HookManager"/>
        /// </summary>
        /// <param name="pipeName">Name of the pipe.</param>
        /// <param name="pid">Process id of the process you want to apply hooks.</param>
        /// <param name="x86dll">Path to the x86 dll.</param>
        /// <param name="x64dll">Path to the x64 dll.</param>
        public HookManager(
            string pipeName,
            int pid,
            string x86dll,
            string x64dll) : this(pipeName, Process.GetProcessById(pid), x86dll, x64dll)
        {
        }

        /// <summary>
        /// Create an instance of <see cref="HookManager"/>
        /// </summary>
        /// <param name="procName">Name of the process you want to apply hooks.</param>
        /// <param name="x86dll">Path to the x86 dll.</param>
        /// <param name="x64dll">Path to the x64 dll.</param>
        public HookManager(
            string procName,
            string x86dll,
            string x64dll)
            : this("WSHook", procName, x86dll, x64dll)
        {
        }

        /// <summary>
        /// Create an instance of <see cref="HookManager"/>
        /// </summary>
        /// <param name="pipeName">Name of the pipe.</param>
        /// <param name="procName">Name of the process you want to apply hooks.</param>
        /// <param name="x86dll">Path to the x86 dll.</param>
        /// <param name="x64dll">Path to the x64 dll.</param>
        public HookManager(
            string pipeName,
            string procName,
            string x86dll,
            string x64dll) : this(pipeName, Process.GetProcessesByName(procName).First(), x86dll, x64dll)
        {
        }

        /// <summary>
        /// Create an instance of <see cref="HookManager"/>
        /// </summary>
        /// <param name="pipeName">Name of the pipe.</param>
        /// <param name="process">The process you want to apply hooks.</param>
        /// <param name="x86dll">Path to the x86 dll.</param>
        /// <param name="x64dll">Path to the x64 dll.</param>
        public HookManager(
            string pipeName,
            Process process,
            string x86dll,
            string x64dll)
        {
            PipeName = pipeName;
            Process = process;

            if (!File.Exists(x86dll))
                throw new FileNotFoundException(null, x86dll);
            X86DLL = x86dll;

            if (!File.Exists(x64dll))
                throw new FileNotFoundException(null, x64dll);
            X64DLL = x64dll;

            _hooks = new List<IHook>();
            ProcessMemory = new RemoteMemory(process);
        }

        /// <summary>
        /// Inject the library specified in the constructor.
        /// </summary>
        /// <typeparam name="T">Injector.</typeparam>
        /// <param name="injectionOptions">Optional options.</param>
        /// <param name="threadCreation">Method to create a thread.</param>
        /// <returns>If the injection succeeds, return true.</returns>
        public bool Inject<T>(
            InjectionOptions injectionOptions = InjectionOptions.None,
            ThreadCreation threadCreation = ThreadCreation.RemoteThread) where T : IInjector
        {
            if (IsInjected) return true;

            Injector = InjectorHelper.CreateInjector<T>(injectionOptions, threadCreation);

            return Injector.Inject(injectionOptions, threadCreation);
        }

        /// <summary>
        /// Inject the library specified in the constructor.
        /// </summary>
        /// <param name="injectionType">Type of injector.</param>
        /// <param name="injectionOptions">Optional options.</param>
        /// <param name="threadCreation">Method to create a thread.</param>
        /// <returns>If the injection succeeds, return true.</returns>
        public bool Inject(InjectionType injectionType, InjectionOptions injectionOptions, ThreadCreation threadCreation)
        {
            if (IsInjected) return true;

            Injector = InjectorHelper.CreateInjector(injectionType, injectionOptions, threadCreation);

            return Injector.Inject(injectionOptions, threadCreation);
        }

        /// <summary>
        /// Create an instance of the specified hook, and return it.
        /// </summary>
        /// <typeparam name="T">Hook type.</typeparam>
        /// <returns>An instance of hook.</returns>
        public T Register<T>() where T : IHook
        {
            var hookInstance = Activator.CreateInstance<T>();
            hookInstance.Init(this);

            _hooks.Add(hookInstance);

            return hookInstance;
        }

        /// <summary>
        /// Register an already created hook instance and return it.
        /// </summary>
        /// <param name="hook">Instance of hook.</param>
        /// <returns>Hook.</returns>
        public IHook Register(IHook hook)
        {
            if (!_hooks.Contains(hook))
                _hooks.Add(hook);
            return hook;
        }

        /// <summary>
        /// Install hooks registered with <see cref="Register{T}"/> and <see cref="Register(IHook)"/>
        /// </summary>
        /// <returns>If hooks are installed, return true.</returns>
        public bool Install()
        {
            if (!IsInjected)
                throw new Exception("Inject the dll first.");

            bool succeed = true;
            _hooks.ForEach(hook =>
            {
                succeed &= hook.Install();
            });

            return succeed;
        }

        public void Dispose()
        {
            if (_hooks != null)
            {
                _hooks.ForEach(hook =>
                {
                    if (hook.IsInstalled)
                        hook.Uninstall();
                });
                _hooks.Clear();
                _hooks = null;
            }

            if (Injector != null)
            {
                if (Injector.IsInjected)
                    Injector.Eject();
                Injector.Dispose();
                Injector = null;
            }
        }
    }
}
