﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using WSHook.Extensions;
using WSHook.Native;
using static WSHook.Native.Ntdll;
using static WSHook.Native.NativeConstants;
using WSHook.Exceptions;

namespace WSHook
{
    public class RemoteMemory : Memory
    {
        private NtReadVirtualMemory _ntReadVirtualMemory;
        private NtAllocateVirtualMemory _ntAllocateVirtualMemory;
        private NtFreeVirtualMemory _ntFreeVirtualMemory;
        private NtProtectVirtualMemory _ntProtectVirtualMemory;
        private NtWriteVirtualMemory _ntWriteVirtualMemory;
        private bool init = false;
        private readonly IntPtr handle;

        public RemoteMemory(Process process) : base(process)
        {
            handle = Process.Handle;
            if (!handle.HasAccess((uint)(PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION)))
            {
                throw new InvalidPermissionException(handle.GetGrantedAccess(), PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION);
            }

            _ntReadVirtualMemory = NativeMethods.Get<NtReadVirtualMemory>();
            _ntWriteVirtualMemory = NativeMethods.Get<NtWriteVirtualMemory>();
            _ntProtectVirtualMemory = NativeMethods.Get<NtProtectVirtualMemory>();
            _ntFreeVirtualMemory = NativeMethods.Get<NtFreeVirtualMemory>();
            _ntAllocateVirtualMemory = NativeMethods.Get<NtAllocateVirtualMemory>();
            init = true;
        }

        public override bool Alloc(ref long address, ref int size, int allocationType, int protect)
        {
            IntPtr ptr = new IntPtr(address);
            bool status = _ntAllocateVirtualMemory(handle, ref ptr, 0, ref size, allocationType, protect);
            address = ptr.ToInt64();
            return status;
        }

        public override void Dispose()
        {
            if (init)
            {
                _ntReadVirtualMemory = null;
                _ntWriteVirtualMemory = null;
                _ntProtectVirtualMemory = null;
                _ntFreeVirtualMemory = null;
                _ntAllocateVirtualMemory = null;
                init = false;
            }
        }

        public override bool Free(long address, int freeType)
        {
            IntPtr ptr = new IntPtr(address);
            return _ntFreeVirtualMemory(handle, ref ptr, out _, freeType);
        }

        public override bool Protect(long address, ref int size, int newProtect, out int oldProtect)
        {
            IntPtr ptr = new IntPtr(address);
            return _ntProtectVirtualMemory(handle, ref ptr, ref size, newProtect, out oldProtect);
        }

        public override bool Read(long address, out byte[] buffer, int size)
        {
            IntPtr ptr = new IntPtr(address);
            return _ntReadVirtualMemory(handle, ptr, out buffer, size, out _);
        }

        public override bool Write(long address, byte[] buffer, int size = 0)
        {
            if (size == 0)
                size = buffer.Length;
            else
                size = Math.Min(buffer.Length, size);

            IntPtr ptr = new IntPtr(address);
            return _ntWriteVirtualMemory(handle, ptr, buffer, size, out _);
        }
    }
}
