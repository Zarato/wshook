﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace WSHook
{
    public abstract class Memory : IDisposable
    {
        public Process Process { get; }

        /// <summary>
        /// Create an instance to access process' memory.
        /// </summary>
        /// <param name="process">Target process.</param>
        protected Memory(Process process) => Process = process;

        public abstract void Dispose();

        /// <summary>
        /// Read memory at the specified address.
        /// </summary>
        /// <param name="address">Base address to read.</param>
        /// <param name="buffer">Content from the specified process address space.</param>
        /// <param name="size">Number of bytes to read.</param>
        /// <returns>If the function succeeds, return true.</returns>
        public abstract bool Read(long address, out byte[] buffer, int size);

        /// <summary>
        /// Write to the specified address.
        /// </summary>
        /// <param name="address">Base address to write.</param>
        /// <param name="buffer">Data to write at the specified process address space.</param>
        /// <param name="size">Optional parameter. Corresponds to the number of bytes to write.</param>
        /// <returns>If the function succeeds, return true.</returns>
        public abstract bool Write(long address, byte[] buffer, int size = 0);

        /// <summary>
        /// Allocate memory at the specified address.
        /// </summary>
        /// <param name="address">Base address. If null, the system will determine where to allocate.</param>
        /// <param name="size">Number of bytes to allocate.</param>
        /// <param name="allocationType">Type of allocation.</param>
        /// <param name="protect">Protection desired.</param>
        /// <returns>If the function succeeds, return true.</returns>
        public abstract bool Alloc(ref long address, ref int size, int allocationType, int protect);

        /// <summary>
        /// Delete a region.
        /// </summary>
        /// <param name="address">Base address.</param>
        /// <returns>If the function succeeds, return true.</returns>
        public abstract bool Free(long address, int freeType);

        /// <summary>
        /// Change the protection on a region.
        /// </summary>
        /// <param name="address">Base address.</param>
        /// <param name="size">Region size.</param>
        /// <param name="newProtect">New protection desired.</param>
        /// <param name="oldProtect">Old protection.</param>
        /// <returns></returns>
        public abstract bool Protect(long address, ref int size, int newProtect, out int oldProtect);

        /// <summary>
        /// Read memory at the specified address.
        /// </summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="address">Base address to read.</param>
        /// <returns>If the function succeeds, return an object.</returns>
        public T Read<T>(long address) where T : struct
        {
            int size = Marshal.SizeOf<T>();

            if (!Read(address, out byte[] buffer, size))
                throw new Win32Exception(Marshal.GetLastWin32Error());


            IntPtr ptrStruct = Marshal.AllocHGlobal(size);
            Marshal.Copy(buffer, 0, ptrStruct, size);
            T obj = Marshal.PtrToStructure<T>(ptrStruct);
            Marshal.FreeHGlobal(ptrStruct);
            return obj;
        }

        /// <summary>
        /// Read a string at the specified address.
        /// </summary>
        /// <param name="address">Base address to read.</param>
        /// <param name="size">Size of the desired string.</param>
        /// <param name="wstring">If true, a char will be 32 bits.</param>
        /// <returns>If the function succeeds, return a string.</returns>
        public string Read(long address, int size, bool wstring = false)
        {
            if (wstring)
                size *= 2;

            if (!Read(address, out byte[] buffer, size))
                throw new Win32Exception(Marshal.GetLastWin32Error());

            if (wstring)
                return Encoding.Unicode.GetString(buffer);

            return Encoding.ASCII.GetString(buffer);
        }

        /// <summary>
        /// Read an array of objects at the specified address.
        /// </summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="address">Base address to read.</param>
        /// <param name="count">Number of objects to read.</param>
        /// <returns>If the function succeeds, return an array of objects.</returns>
        public T[] Read<T>(long address, int count) where T : struct
        {
            T[] array = new T[count];
            int size = Marshal.SizeOf<T>();
            for(int i = 0; i < count; i++)
            {
                array[i] = Read<T>(address);
                address += size;
            }
            return array;
        }

        /// <summary>
        /// Write an object to the specified address.
        /// </summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="address">Base address to write.</param>
        /// <param name="obj">Object.</param>
        /// <returns>If the function succeeds, return true.</returns>
        public bool Write<T>(long address, T obj) where T : struct
        {
            int size = Marshal.SizeOf<T>();
            byte[] buffer = new byte[size];

            IntPtr structPtr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(obj, structPtr, true);
            Marshal.Copy(structPtr, buffer, 0, size);
            Marshal.FreeHGlobal(structPtr);

            return Write(address, buffer, size);
        }

        /// <summary>
        /// Write a string to the specified address.
        /// </summary>
        /// <param name="address">Base address to write.</param>
        /// <param name="text">Text to write.</param>
        /// <param name="encoding">Optional. Encoding to use.</param>
        /// <returns>If the function succeeds, return true.</returns>
        public bool Write(long address, string text, Encoding encoding = null)
        {
            if (encoding == null)
                encoding = Encoding.UTF8;

            return Write(address, encoding.GetBytes(text));
        }

        /// <summary>
        /// Write an array of objects to the specified address;
        /// </summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="address">Base address to write.</param>
        /// <param name="arr">Array of objects.</param>
        /// <returns>If the function succeeds, return true.</returns>
        public bool Write<T>(long address, T[] arr) where T : struct
        {
            int size = Marshal.SizeOf<T>();
            bool success = true;
            for (int i = 0; i < arr.Length; i++)
            {
                success &= Write<T>(address, arr[i]);
            }
            return success;
        }

    }
}
