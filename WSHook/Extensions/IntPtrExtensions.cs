﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;
using WSHook.Native;
using static WSHook.Native.NativeStructs;
using static WSHook.Native.Ntdll;

namespace WSHook.Extensions
{
    internal static class IntPtrExtensions
    {

        /// <summary>
        /// Get Granted Access of the handle.
        /// </summary>
        /// <param name="ptr">Handle.</param>
        /// <returns>Granted Access.</returns>
        public static uint GetGrantedAccess(this IntPtr ptr)
        {
            int size = Marshal.SizeOf<PUBLIC_OBJECT_BASIC_INFORMATION>();
            IntPtr objInformationPtr = Marshal.AllocHGlobal(size);

            NtQueryObject ntQueryObject = NativeMethods.Get<NtQueryObject>();
            if (!ntQueryObject(
                ptr,
                NativeConstants.OBJECT_INFORMATION_CLASS.ObjectBasicInformation,
                objInformationPtr,
                (uint)size,
                out uint cbResult))
            {
                Marshal.FreeHGlobal(objInformationPtr);
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }

            PUBLIC_OBJECT_BASIC_INFORMATION objInformation = Marshal.PtrToStructure<PUBLIC_OBJECT_BASIC_INFORMATION>(objInformationPtr);

            uint grantedAccess = objInformation.GrantedAccess;
            Marshal.FreeHGlobal(objInformationPtr);

            return grantedAccess;
        }

        /// <summary>
        /// Check if the handle has the specified access.
        /// </summary>
        /// <param name="ptr">Handle.</param>
        /// <param name="access">Access.</param>
        /// <returns>Return true if the handle has the access.</returns>
        public static bool HasAccess(this IntPtr ptr, uint access) => (ptr.GetGrantedAccess() & access) == access;

    }
}
