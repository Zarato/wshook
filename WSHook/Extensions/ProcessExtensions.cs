﻿using System;
using System.Diagnostics;
using WSHook.Native;
using static WSHook.Native.Kernel32;

namespace WSHook.Extensions
{
    internal static class ProcessExtensions
    {
        public static bool Is64Bit(this Process process)
        {
            if (process.Id == Process.GetCurrentProcess().Id)
                return IntPtr.Size == 8;

            if ((Environment.OSVersion.Version.Major > 5)
                || ((Environment.OSVersion.Version.Major == 5) && (Environment.OSVersion.Version.Minor >= 1)))
            {

                return NativeMethods.Get<IsWow64Process>()(process.Handle, out bool retVal)
                       && retVal;
            }

            return false; // not on 64-bit Windows Emulator
        }

    }
}
