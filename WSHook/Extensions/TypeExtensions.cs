﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace WSHook.Extensions
{
    internal static class TypeExtensions
    {

        /// <summary>
        /// Indicates if the type has the specified attribute.
        /// </summary>
        /// <typeparam name="T">Attribute.</typeparam>
        /// <param name="type">Type.</param>
        /// <returns>Return true if the type has the attribute.</returns>
        public static bool HasAttribute<T>(this Type type) where T : Attribute => type.GetCustomAttribute<T>() != null;

    }
}
