﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Text;

namespace WSHook
{
    // From https://github.com/thenameless314159/Astron/blob/master/src/Astron.Memory/SimpleMemoryOwner.cs
    public sealed class SimpleMemoryOwner<T> : IMemoryOwner<T>
    {
        public static IMemoryOwner<T> Empty { get; } = new SimpleMemoryOwner<T>(Array.Empty<T>());
        public SimpleMemoryOwner(Memory<T> memory) => Memory = memory;

        public Memory<T> Memory { get; }
        public void Dispose() { }
    }
}
