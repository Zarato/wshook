﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace WSHook.Hooks
{
    public interface IHook : IDisposable
    {
        /// <summary>
        /// Register the hook manager to prepare hooking.
        /// </summary>
        /// <param name="hookManager">Hook Manager for the process you want to inject the dll to.</param>
        void Init(HookManager hookManager);

        /// <summary>
        /// Request the DLL to install the hook.
        /// </summary>
        /// <returns>If installation succeeds, return true.</returns>
        bool Install();

        /// <summary>
        /// Request the DLL to uninstall the hook.
        /// </summary>
        /// <returns>If the hooks is removed, return true.</returns>
        bool Uninstall();

        /// <summary>
        /// Indicates if the hook is installed.
        /// </summary>
        bool IsInstalled { get; }
    }
}
