﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSHook.Injection
{
    public interface IInjector : IDisposable
    {
        /// <summary>
        /// Register the hook manager to prepare injection.
        /// </summary>
        /// <param name="hookManager">Hook Manager for the process you want to inject the dll to.</param>
        void Init(HookManager hookManager);

        /// <summary>
        /// Inject the DLL into the process specified in the hook manager.
        /// </summary>
        /// <param name="options">Additionnal options.</param>
        /// <param name="creation">The method to create a thread.</param>
        /// <returns>If injection succeeds, return true.</returns>
        bool Inject(InjectionOptions options = InjectionOptions.None, ThreadCreation creation = ThreadCreation.RemoteThread);

        /// <summary>
        /// Remove the DLL from the process specified in the hook manager.
        /// </summary>
        /// <returns>If ejection succeeds, return true.</returns>
        bool Eject();

        /// <summary>
        /// Indicates if the dll is injected.
        /// </summary>
        public bool IsInjected { get; }
    }
}
