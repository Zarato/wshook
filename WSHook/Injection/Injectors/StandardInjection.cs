﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSHook.Injection
{
    [Injector(InjectionType.Standard)]
    public class StandardInjection : IInjector
    {
        public bool IsInjected => throw new NotImplementedException();

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool Eject()
        {
            throw new NotImplementedException();
        }

        public void Init(HookManager hookManager)
        {
            throw new NotImplementedException();
        }

        public bool Inject(InjectionOptions options = InjectionOptions.None, ThreadCreation creation = ThreadCreation.RemoteThread)
        {
            throw new NotImplementedException();
        }
    }
}
