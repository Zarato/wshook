﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using WSHook.Exceptions;
using WSHook.Extensions;

namespace WSHook.Injection
{
    internal static class InjectorHelper
    {
        private static Dictionary<InjectionType, Type> injectors;

        static InjectorHelper()
        {
            // retrieve all injectors in the current assembly
            var types = Assembly.GetExecutingAssembly()
                                .GetTypes()
                                .Where(t => t.IsAssignableFrom(typeof(IInjector)) && t.HasAttribute<InjectorAttribute>());
            injectors = new Dictionary<InjectionType, Type>();

            foreach (var injector in types)
            {
                InjectionType injectionType = injector.GetCustomAttribute<InjectorAttribute>().InjectionType;
                injectors.Add(injectionType, injector);
            }
        }

        /// <summary>
        /// Create an injector and check if the specified options are supported.
        /// </summary>
        /// <param name="injectionType">Method to inject the dll.</param>
        /// <param name="options">Optional options.</param>
        /// <param name="threadCreation">Method to create a thread.</param>
        /// <returns>An injector instance.</returns>
        public static IInjector CreateInjector(InjectionType injectionType, InjectionOptions options = InjectionOptions.None, ThreadCreation threadCreation = ThreadCreation.RemoteThread)
        {
            if (!injectors.ContainsKey(injectionType))
                throw new KeyNotFoundException($"Injector for {injectionType.ToString()} doesn't exist.");

            Type injector = injectors[injectionType];

            if (options != InjectionOptions.None)
            {
                if (!IsSupported(injector, options))
                    throw new NotSupportedException($"{injector.Name} doesn't support the specified options.");
            }

            if (!IsSupported(injector, threadCreation))
                throw new NotSupportedException($"{injector.Name} doesn't support the Thread Creation method.");

            return Activator.CreateInstance(injector) as IInjector;
        }


        /// <summary>
        /// Create an injector and check if the specified options are supported.
        /// </summary>
        /// <typeparam name="T">Type of injector.</typeparam>
        /// <param name="options">Optional options.</param>
        /// <param name="threadCreation">Method to create a thread.</param>
        /// <returns>An injector instance.</returns>
        public static T CreateInjector<T>(InjectionOptions options = InjectionOptions.None, ThreadCreation threadCreation = ThreadCreation.RemoteThread) where T : IInjector
        {
            if (options != InjectionOptions.None)
            {
                if (!IsSupported<T>(options))
                    throw new NotSupportedException($"{typeof(T).Name} doesn't support the specified options.");
            }

            if (!IsSupported<T>(threadCreation))
                throw new NotSupportedException($"{typeof(T).Name} doesn't support the Thread Creation method.");


            return (T)Activator.CreateInstance<T>();
        }


        /// <summary>
        /// Check if the injector supports the specified options.
        /// </summary>
        /// <param name="injector">Injector type.</param>
        /// <param name="options">Options.</param>
        /// <returns>If the injector supports the options, return true.</returns>
        public static bool IsSupported(Type injector, InjectionOptions options)
        {
            if (!injector.HasAttribute<InjectorAttribute>())
                throw new InvalidInjectorException(injector);

            var injectorOptions = injector.GetCustomAttribute<InjectorAttribute>().SupportedOptions;

            return (injectorOptions & options) == options;
        }


        /// <summary>
        /// Check if the injector supports the specified options.
        /// </summary>
        /// <typeparam name="T">Injector type.</typeparam>
        /// <param name="options">Options.</param>
        /// <returns>If the injector supports the options, return true.</returns>
        public static bool IsSupported<T>(InjectionOptions options) where T : IInjector => IsSupported(typeof(T), options);


        /// <summary>
        /// Check if the injector supports the specified thread creation method.
        /// </summary>
        /// <param name="injector">Injector type.</param>
        /// <param name="creation">Thread creation method.</param>
        /// <returns>If the injector supports the method, return true.</returns>
        public static bool IsSupported(Type injector, ThreadCreation creation)
        {
            if (!injector.HasAttribute<InjectorAttribute>())
                throw new InvalidInjectorException(injector);

            var injectorCreation = injector.GetCustomAttribute<InjectorAttribute>().SupportedThreadCreation;

            return (injectorCreation & creation) == creation;
        }

        /// <summary>
        /// Check if the injector supports the specified thread creation method.
        /// </summary>
        /// <typeparam name="T">Injector type.</typeparam>
        /// <param name="creation">Thread creation method.</param>
        /// <returns>If the injector supports the method, return true.</returns>
        public static bool IsSupported<T>(ThreadCreation creation) where T : IInjector => IsSupported(typeof(T), creation);
    }
}
