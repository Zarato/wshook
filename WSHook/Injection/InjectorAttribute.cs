﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSHook.Injection
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectorAttribute : Attribute
    {
        public InjectionType InjectionType { get; }
        public InjectionOptions SupportedOptions { get; }
        public ThreadCreation SupportedThreadCreation { get; }

        public InjectorAttribute(
            InjectionType injectionType,
            InjectionOptions injectionOptions = InjectionOptions.None,
            ThreadCreation threadCreation = ThreadCreation.RemoteThread)
        {
            InjectionType = injectionType;
            SupportedOptions = injectionOptions;
            SupportedThreadCreation = threadCreation;
        }
    }
}
