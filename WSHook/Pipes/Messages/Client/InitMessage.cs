﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSHook.Pipes.Messages.Client
{
    [PipeMessage(2)]
    public class InitMessage : IMessage
    {
        public int ProcessId { get; set; }
    }
}
