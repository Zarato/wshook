﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using WSHook.Extensions;

namespace WSHook.Pipes.Messages
{
    internal static class MessagesManager
    {
        private static Dictionary<Type, Func<IMessage, byte[]>> _serializers;

        static MessagesManager()
        {
            _serializers = new Dictionary<Type, Func<IMessage, byte[]>>();
            // get messages
            var types = Assembly.GetExecutingAssembly()
                                .GetTypes()
                                .Where(t => t.IsAssignableFrom(typeof(IMessage)) && t.HasAttribute<PipeMessageAttribute>());
 
            foreach (var message in types)
            {
                // build a serializer
            }
        }
    }
}
