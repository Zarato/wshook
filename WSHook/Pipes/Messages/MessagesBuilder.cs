﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using WSHook.Extensions;
using WSHook.IO;
using WSHook.IO.Cache;
using WSHook.IO.Writer;

namespace WSHook.Pipes.Messages
{
    public static class MessagesBuilder
    {
        public static Func<IMessage, byte[]> BuildSerializer(Type type, IBinaryFactory binaryFactory)
        {
            if (!type.HasAttribute<PipeMessageAttribute>())
                throw new ArgumentException();

            Func<IMessage, int> sizeOfMessage = BuildSizeOf(type);

            // create serializer
            ParameterExpression parameter = Expression.Parameter(typeof(IMessage), "message");
            ParameterExpression sizeVariable = Expression.Variable(typeof(int), "size");

            var messageParam = Expression.Convert(parameter, type);

            var initSize = Expression.Assign(sizeVariable,
                                             Expression.Call(sizeOfMessage.Method, messageParam));

            var factory = Expression.Constant(binaryFactory);
            var writer = Expression.Call(factory,
                                         typeof(BinaryFactory).GetMethod(nameof(BinaryFactory.Get),
                                                                         new Type[] { typeof(int) }),
                                         sizeVariable);
            var memwriter = Expression.Convert(writer, typeof(MemoryWriter));

            List<Expression> exprSerializer = new List<Expression>();
            exprSerializer.Add(initSize);
            exprSerializer.Add(writer);

            var memoryWriterMethods = typeof(MemoryWriter).GetMethods().Where(m => m.GetParameters().Length == 1);
            var writeValueMethod = memoryWriterMethods.First(m => !m.GetParameters()[0].ParameterType.IsArray);
            var writeArrayMethod = memoryWriterMethods.First(m => m.GetParameters()[0].ParameterType.IsArray);

            foreach (var property in type.GetProperties())
            {
                Type propType = property.PropertyType;
                // TODO : array
                var exprProperty = Expression.Property(messageParam, property);
                if (propType.IsArray)
                {
                    exprSerializer.Add(Expression.Call(memwriter, writeArrayMethod.MakeGenericMethod(propType), exprProperty));
                }
                else
                {
                    exprSerializer.Add(Expression.Call(memwriter, writeValueMethod.MakeGenericMethod(propType), exprProperty));
                }
            }

            var bufferMem = Expression.Call(memwriter, typeof(MemoryWriter).GetMethod(nameof(MemoryWriter.GetBuffer)));
            var buffer = Expression.Call(bufferMem, typeof(Memory<byte>).GetMethod(nameof(Memory<byte>.ToArray)));
            exprSerializer.Add(buffer);

            var blockExpr = Expression.Block(
                new[] {sizeVariable}, exprSerializer
                );

            return Expression.Lambda<Func<IMessage, byte[]>>(blockExpr, parameter).Compile();
        }

        public static Func<IMessage, int> BuildSizeOf(Type type)
        {
            // TODO : array
            DynamicMethod dynamicMethod = new DynamicMethod("SizeOfMessage",
                                                            typeof(int),
                                                            new Type[] { typeof(IMessage) });
            ILGenerator generator = dynamicMethod.GetILGenerator();
            generator.Emit(OpCodes.Ldc_I4_0); // push 0 to the stack
            foreach(var property in type.GetProperties())
            {
                Type propType = property.PropertyType;
                if (PrimitiveTypes.Primitives.Contains(property.PropertyType))
                {
                    var sizeofMethod = typeof(PrimitiveBinaryCache<>).MakeGenericType(propType)
                                                                     .GetProperty("SizeOf", BindingFlags.Static | BindingFlags.Public)
                                                                     .GetGetMethod();
                    generator.Emit(OpCodes.Call, sizeofMethod); // get property PrimitiveBinaryCache<propType>.SizeOf and push the result to the stack
                }
                else
                {
                    // get sizeof property
                    var sizeofProp = typeof(BinaryCache<>).MakeGenericType(propType)
                                                          .GetProperty("SizeOf", BindingFlags.Static | BindingFlags.Public);
                    var sizeofMethod = sizeofProp.GetGetMethod();
                    generator.Emit(OpCodes.Call, sizeofMethod); // call PrimitiveBinaryCache<propType>.SizeOf and push the result to the stack

                    // load argument
                    generator.Emit(OpCodes.Ldarg_0);
                    // get property value and push to the stack
                    generator.Emit(OpCodes.Call, property.GetGetMethod());

                    // call SizeOf
                    generator.Emit(OpCodes.Callvirt, sizeofProp.PropertyType.GetMethod("Invoke"));
                }
                generator.Emit(OpCodes.Add); // add the 2 integer at the stop of the stack, and push the result
            }
            generator.Emit(OpCodes.Ret);

            dynamicMethod.DefineParameter(1, ParameterAttributes.In, "message");

            return dynamicMethod.CreateDelegate(typeof(Func<IMessage, int>)) as Func<IMessage, int>;
        }

    }
}
