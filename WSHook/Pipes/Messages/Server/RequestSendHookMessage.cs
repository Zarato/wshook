﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSHook.Pipes.Messages.Server
{
    [PipeMessage(1)]
    public class RequestSendHookMessage : IMessage
    {
        public bool Install { get; set; }
    }
}
