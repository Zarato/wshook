﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSHook.Pipes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PipeMessageAttribute : Attribute
    {
        public int Id { get; }
        public PipeMessageAttribute(int id)
        {
            Id = id;
        }
    }
}
