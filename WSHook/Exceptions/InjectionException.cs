﻿using System;
using System.Collections.Generic;
using System.Text;
using WSHook.Injection;

namespace WSHook.Exceptions
{
    public class InjectionException : Exception
    {
        public InjectionException(IInjector injector) : base($"Can't inject the dll with the injector {injector.GetType().Name}.")
        {
        }
    }
}
