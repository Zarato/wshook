﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace WSHook.Exceptions
{
    public class InvalidInjectorException : Exception
    {
        public InvalidInjectorException(Type type) : this($"Injector {type.Name} is not valid.")
        {
        }

        public InvalidInjectorException(string message) : base(message)
        {
        }
    }
}
