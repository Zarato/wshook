﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSHook.Exceptions
{
    public class InvalidPermissionException : Exception
    {
        public InvalidPermissionException(
            object permissions,
            object expected) : base($"Invalid permissions {permissions}, expected: {expected}.")
        {
        }
    }
}
