﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSHook.Exceptions
{
    public class InvalidMethodException : Exception
    {
        public InvalidMethodException(string method, string module) : base($"Method {method} doesn't exist in module {module}")
        {
        }
    }
}
