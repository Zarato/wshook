﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSHook.Exceptions
{
    public class InvalidModuleException : Exception
    {
        public InvalidModuleException(string module) : base($"Module {module} doesn't exist")
        {
        }
    }
}
