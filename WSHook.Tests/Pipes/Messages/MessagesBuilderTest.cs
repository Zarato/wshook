﻿using System;
using System.Collections.Generic;
using System.Text;
using WSHook.IO;
using WSHook.IO.Storage;
using WSHook.Pipes.Messages;
using Xunit;

namespace WSHook.Tests.Pipes.Messages
{
    public class MessagesBuilderTest : IClassFixture<MessageFixture>
    {
        MessageFixture fixture;

        public MessagesBuilderTest(MessageFixture fixture) => this.fixture = fixture;

        [Fact]
        public void BuildSizeOfTest()
        {
            Func<MessageTest, int> func = MessagesBuilder.BuildSizeOf(typeof(MessageTest));
            var size = sizeof(int) + fixture.Message.Param2.Length;
            Assert.Equal(size, func(fixture.Message));
        }

        [Fact]
        public void BuildSerializerTest()
        {
            Func<MessageTest, byte[]> func = MessagesBuilder.BuildSerializer(typeof(MessageTest), fixture.Factory);
            var writer = fixture.Factory.Get(4 + fixture.Message.Param2.Length + 4);
            writer.WriteValue(fixture.Message.Param1);
            writer.WriteValue(fixture.Message.Param2);

            var buffer = writer.GetBuffer().ToArray();
            Assert.Equal(buffer, func(fixture.Message));
        }
    }
}
