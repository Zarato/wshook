﻿using System;
using System.Collections.Generic;
using System.Text;
using WSHook.IO;
using WSHook.IO.Storage;
using WSHook.Pipes;
using WSHook.Pipes.Messages;

namespace WSHook.Tests.Pipes.Messages
{
    [PipeMessage(1)]
    public class MessageTest : IMessage
    {
        public int Param1 { get; set; }
        public string Param2 { get; set; }
    }

    public class MessageFixture : IDisposable
    {
        public BinaryBuilder Builder { get; private set; }
        public BinaryFactory Factory { get; private set; }
        public MessageTest Message { get; private set; } = new MessageTest
        {
            Param1 = 1,
            Param2 = "Test"
        };

        public MessageFixture()
        {
            Builder = new BinaryBuilder();
            Builder.Register(new AsciiBinaryStorage());
            Builder.Register<string>(s => s.Length);
            Factory = Builder.Build() as BinaryFactory;
        }

        public void Dispose()
        {
            Factory = null;
            Builder = null;
            Message = null;
        }
    }
}
